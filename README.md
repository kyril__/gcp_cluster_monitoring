# GKE cluster with monitoring ![image](https://www.medimsight.com/media/assets/images/gcp_icon.png) ![image](https://res.cloudinary.com/canonical/image/fetch/f_auto,q_auto,fl_sanitize,w_48,h_48/https://dashboard.snapcraft.io/site_media/appmedia/2019/11/terraform.png)

## List of resources
- Terraform files for setup GKE cluster
- Python web-server
- Prometheus resources
- Grafana resources

## Usage
- Install terraform/opentofu from official site
### Init commands
```bash
terraform init
terraform plan
terraform apply
```
### Gaining access to GKE cluster

```bash
gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)
```
### Trigger scripts from folders/use Kubernetes terraform provider for creating resources:
#### Web-server
```bash
./flask_init.sh
```
#### Prometheus
```bash
#from config folder
./cfg_init.sh 
./prom_init.sh
```
#### Grafana
```bash
./grafana_init.sh
```
## Web server endpoints
- **/message**  (return 200 & formatted info from vm enviroment)
- **/health**   (return 200)
- **/client_error**  (return 404)
- **/server_error** (return 500) 
### Test Load command
```bash
for ((i=1;i<=100;i++)); do curl -X GET http://127.0.0.1:5000/message ;done 
#Change limit @ endpoint for test
```
## GKE cluster reference 
### Ingress nginx Install command
```bash
kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole cluster-admin \
  --user $(gcloud config get-value account)

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.10.1/deploy/static/provider/cloud/deploy.yaml
```
### Cluster schema
![image](media/gcp-schema.png)
### Cluster screenshots
![image](media/image_2024-06-09_12-33-53.png?ref_type=heads)
![image](media/image_2024-06-09_12-34-25.png?ref_type=heads)
![image](media/image_2024-06-09_15-16-03.png?ref_type=heads)
## Dashboard & Alert reference
### Alert reference
```yaml
# alert.rules
groups:
- name: example
  rules:
  - alert: HighErrorRate
    expr: rate(http_requests_total{method="get", status=~"5.."}[5m]) / rate(http_requests_total{method="get"}[5m]) > 0.1
    for: 10m
    labels:
      severity: page
    annotations:
      summary: High request error rate detected
      description: "More than 10% of GET requests are failing over the last 5 minutes"

```
### Dashboard visualisation
![image](media/dashboard.png?ref_type=heads)
![image](media/image_2024-06-09_16-00-54.png?ref_type=heads)