#!/bin/bash
echo "Build Docker image"
docker build -t myregistry:flask_api:sometag .
echo "Push to Docker Hub"
docker push myregistry:flask_api:sometag
echo "Deploy by Helm chart"
helm install --kubeconfig=KUBECONFIG flask-release ./helm-chart

